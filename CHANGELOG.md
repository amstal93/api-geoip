# CHANGELOG

## 0.6

### Features

* Rota `/metrics/` para coleta de métricas com Prometheus.

## 0.3

### Features

* Configuração de caminho do log da aplicação, default `app.log` na execução com python, e `/opt/geoip/logs` com Docker.

## 0.2

### Feature

* Requisições geram logs com o IP de quem solicitou e o IP que deseja obter a geolocalização.
* Nova rota `/uptime`. Retorna uptime da API.
* Remoção da rota `/`. Antiga rota que retornava uptime da API.

## 0.1

### Features

* Rota `/v1/{ipv4}` que retorna a geolocalização em `json`. (Sem tratamento a exceções e sem a utilização dos parametros opcionais)
